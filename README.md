# fun7

## Prerequisites 

Application is consisted of two parts:

* application: fun7-app 
* database: fun7-db

Both parts are packaged with Docker and communicate with each other through network bridge `most`. You can create that using the following command:

`docker network create most`

fun7-app uses PostgreSQL database, which is packaged in Docker container. Docker container with the right settings can be obtained:

`docker run --net=most --name fun7-db -e POSTGRES_PASSWORD=42 -d -p 5432:5432 postgres`

## Build

Application is managed and built using Maven. Clean build can be obtained by

`mvn clean package`

During build Maven also runs unit tests.

When you've build an application you have to create a Docker image. This directory already contains required Dockerfile, therefore you only need to run:

`docker build -t fun7 .`

To run the Docker container with the correct setting use the following command:

`docker run --net=most --name fun7-app -p 8080:8080 -d fun7`

## Use

Application is available on `localhost:8080`. Application exposes two REST APIs:

* `/v1/check`
* `/v1/admin/users`

The documentation/specification of the APIs together with list of available endpoints is available on `localhost:8080/api-specs/ui`. 

## Tests

### Unit tests

Unit tests were written using JUnit. They can be run with:

`mvn clean test`

Overall there are just 3 unit tests, since the focus was on the integration tests. This is because most of the functionality needed access to the database.

### Integration tests

Integration tests were written using Arquillian testing framework. It also uses KumuluzEE Arquillian adapter in order to run the application in the KumuluzEE container. The integration tests can be run with:

`mvn clean verify`

Overall there are 14 integration tests.