FROM openjdk:11-jre-slim
COPY . /fun7
WORKDIR fun7
EXPOSE 8080
CMD ["java", "-jar", "./target/fun7-1.0.0.jar"]