package processing;

import beans.UserBean;
import com.kumuluz.ee.configuration.utils.ConfigurationUtil;
import entities.User;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.*;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(Arquillian.class)
public class CheckServicesIT {


    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClasses(CheckServices.class, UserBean.class, StateDTO.class, User.class)
                .addAsManifestResource("META-INF/beans.xml", "beans.xml")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsResource("config.yaml", "config.yaml")
                .addAsResource("sql-scripts/init-db.sql", "sql-scripts/init-db.sql")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    CheckServices checkServices;

    @Test
    public void checkServicesTest(){

        Clock testClock = Clock.fixed(LocalDateTime.of(2021, 6, 6, 8, 50).toInstant(OffsetDateTime.now().getOffset()), Clock.systemDefaultZone().getZone());
        checkServices.setClock(testClock);

        StateDTO testStateDTO = new StateDTO();
        testStateDTO.setUserSupport("disabled");
        testStateDTO.setMultiplayer("disabled");

        assertEquals(testStateDTO, checkServices.checkServices("Europe/Ljubljana", "zani", "US"));

        checkServices.initDefaultClock();
    }

    @Test
    public void checkServicesTest1(){

        Clock testClock = Clock.fixed(LocalDateTime.of(2021, 6, 6, 8, 50).toInstant(OffsetDateTime.now().getOffset()), Clock.systemDefaultZone().getZone());
        checkServices.setClock(testClock);

        StateDTO testStateDTO = new StateDTO();
        testStateDTO.setUserSupport("disabled");
        testStateDTO.setMultiplayer("disabled");

        assertEquals(testStateDTO, checkServices.checkServices("Europe/Ljubljana", "skilled_user", "SI"));

        checkServices.initDefaultClock();
    }

    @Test
    public void checkServicesTest2(){

        Clock testClock = Clock.fixed(LocalDateTime.of(2021, 6, 6, 8, 50).toInstant(OffsetDateTime.now().getOffset()), Clock.systemDefaultZone().getZone());
        checkServices.setClock(testClock);

        StateDTO testStateDTO = new StateDTO();
        testStateDTO.setUserSupport("disabled");
        testStateDTO.setMultiplayer("enabled");

        assertEquals(testStateDTO, checkServices.checkServices("Europe/Ljubljana", "skilled_user", "US"));

        checkServices.initDefaultClock();
    }

    @Test
    public void checkServicesTest3(){

        Clock testClock = Clock.fixed(LocalDateTime.of(2021, 6, 6, 8, 50).toInstant(OffsetDateTime.now().getOffset()), Clock.systemDefaultZone().getZone());
        checkServices.setClock(testClock);

        StateDTO testStateDTO = new StateDTO();
        testStateDTO.setUserSupport("disabled");
        testStateDTO.setMultiplayer("disabled");

        assertEquals(testStateDTO, checkServices.checkServices("America/New_York", "first_user", "US"));
        assertEquals(testStateDTO, checkServices.checkServices("America/New_York", "first_user", "US"));
        testStateDTO.setMultiplayer("enabled");
        assertEquals(testStateDTO, checkServices.checkServices("America/New_York", "first_user", "US"));

        checkServices.initDefaultClock();
    }

    @Test
    public void checkServicesTest4(){

        Clock testClock = Clock.fixed(LocalDateTime.of(2021, 6, 4, 12, 50).toInstant(OffsetDateTime.now().getOffset()), Clock.systemDefaultZone().getZone());
        checkServices.setClock(testClock);

        StateDTO testStateDTO = new StateDTO();
        testStateDTO.setUserSupport("enabled");
        testStateDTO.setMultiplayer("disabled");

        assertEquals(testStateDTO, checkServices.checkServices("America/New_York", "clock_user", "US"));

        checkServices.initDefaultClock();
    }

    @Test
    public void checkServicesTest5(){

        Clock testClock = Clock.fixed(LocalDateTime.of(2021, 6, 5, 12, 50).toInstant(OffsetDateTime.now().getOffset()), Clock.systemDefaultZone().getZone());
        checkServices.setClock(testClock);

        StateDTO testStateDTO = new StateDTO();
        testStateDTO.setUserSupport("disabled");
        testStateDTO.setMultiplayer("disabled");

        assertEquals(testStateDTO, checkServices.checkServices("America/New_York", "clock_user", "US"));

        checkServices.initDefaultClock();
    }

    @Test
    public void checkServicesTest6(){

        Clock testClock = Clock.fixed(LocalDateTime.of(2021, 6, 5, 19, 50).toInstant(OffsetDateTime.now().getOffset()), Clock.systemDefaultZone().getZone());
        checkServices.setClock(testClock);

        StateDTO testStateDTO = new StateDTO();
        testStateDTO.setUserSupport("disabled");
        testStateDTO.setMultiplayer("disabled");

        assertEquals(testStateDTO, checkServices.checkServices("America/New_York", "clock_user", "US"));

        checkServices.initDefaultClock();
    }



}
