package api.v1.sources;

import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.*;


public class UserSourceTest {

    @Test
    public void getUserDetailsTest(){
        UserSource userSource = new UserSource();
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), userSource.getUserDetails(null).getStatusInfo().getStatusCode());
    }

    @Test
    public void deleteUserTest(){
        UserSource userSource = new UserSource();
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), userSource.deleteUser(null).getStatusInfo().getStatusCode());
    }


}
