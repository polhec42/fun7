package api.v1.sources;

import beans.UserBean;
import entities.User;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import processing.CheckServices;
import processing.StateDTO;

import javax.inject.Inject;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Arquillian.class)
public class CheckServicesSourceIT {


    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClasses(CheckServices.class, CheckServicesSource.class, UserBean.class, StateDTO.class, User.class)
                .addAsManifestResource("META-INF/beans.xml", "beans.xml")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsResource("config.yaml", "config.yaml")
                .addAsResource("sql-scripts/init-db.sql", "sql-scripts/init-db.sql")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    CheckServicesSource checkServicesSource;

    @Inject
    CheckServices checkServices;

    @Test
    public void checkServicesSourceTest(){

        Clock testClock = Clock.fixed(LocalDateTime.of(2021, 6, 6, 8, 50).toInstant(OffsetDateTime.now().getOffset()), Clock.systemDefaultZone().getZone());
        checkServices.setClock(testClock);

        StateDTO testStateDTO = new StateDTO();
        testStateDTO.setUserSupport("disabled");
        testStateDTO.setMultiplayer("disabled");

        assertEquals(testStateDTO, checkServicesSource.checkServices("Europe/Ljubljana", "zani", "US").getEntity());

        checkServices.initDefaultClock();
    }

    @Test
    public void checkServicesSourceTest2(){

        Clock testClock = Clock.fixed(LocalDateTime.of(2021, 6, 3, 12, 50).toInstant(OffsetDateTime.now().getOffset()), Clock.systemDefaultZone().getZone());
        checkServices.setClock(testClock);

        StateDTO testStateDTO = new StateDTO();
        testStateDTO.setUserSupport("enabled");
        testStateDTO.setMultiplayer("disabled");

        assertEquals(testStateDTO, checkServicesSource.checkServices("Europe/Ljubljana", "zani", "US").getEntity());

        checkServices.initDefaultClock();
    }
}