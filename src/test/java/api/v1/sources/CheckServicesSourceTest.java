package api.v1.sources;

import jdk.jfr.Name;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import javax.ws.rs.core.Response;
import static org.junit.jupiter.api.Assertions.*;


public class CheckServicesSourceTest {

    @Test
    public void checkServicesTest(){
        CheckServicesSource checkServicesSource = new CheckServicesSource();

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), checkServicesSource.checkServices(null,null,null).getStatusInfo().getStatusCode());
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), checkServicesSource.checkServices("Europe/Ljubljana",null,null).getStatusInfo().getStatusCode());
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), checkServicesSource.checkServices(null,"UK",null).getStatusInfo().getStatusCode());
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), checkServicesSource.checkServices(null, null,"testId").getStatusInfo().getStatusCode());
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), checkServicesSource.checkServices("Europe/Ljubljana","UK",null).getStatusInfo().getStatusCode());
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), checkServicesSource.checkServices("Europe/Ljubljana",null,"testId").getStatusInfo().getStatusCode());
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), checkServicesSource.checkServices(null,"UK","testId").getStatusInfo().getStatusCode());
    }

}
