package api.v1.sources;

import beans.UserBean;
import entities.User;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import processing.CheckServices;
import processing.StateDTO;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Arquillian.class)
public class UserSourceIT {


    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClasses(UserSource.class, UserBean.class, User.class)
                .addAsManifestResource("META-INF/beans.xml", "beans.xml")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsResource("config.yaml", "config.yaml")
                .addAsResource("sql-scripts/init-db.sql", "sql-scripts/init-db.sql")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    UserSource userSource;

    @Test
    public void getUsers(){
        assertEquals(3, ((List<User>)(userSource.getUsers().getEntity())).size()); //We've deleted one user to test functionality
    }

    @Test
    public void getUser(){
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), userSource.getUserDetails("unknown").getStatusInfo().getStatusCode());
    }

    @Test
    public void getUser2(){
        User user = new User();
        user.setUserId("skilled_user");
        user.setCc("SI");
        user.setTimezone("Europe/Ljubljana");

        assertEquals(user, userSource.getUserDetails("skilled_user").getEntity());
    }

    @Test
    public void deleteUser(){
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), userSource.getUserDetails("unknown").getStatusInfo().getStatusCode());
    }

    @Test
    public void deleteUser2(){
        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), userSource.deleteUser("oil_user").getStatusInfo().getStatusCode());
    }
}
