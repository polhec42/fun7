INSERT INTO users (timezone, userId, cc, numberOfUses) VALUES ('America/New_York', 'first_user', 'US', 3);
INSERT INTO users (timezone, userId, cc, numberOfUses) VALUES ('Asia/Qatar', 'oil_user', 'QA', 7);
INSERT INTO users (timezone, userId, cc, numberOfUses) VALUES ('Europe/Ljubljana', 'skilled_user', 'SI', 12);
