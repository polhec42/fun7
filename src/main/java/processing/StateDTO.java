package processing;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.swing.plaf.nimbus.State;

public class StateDTO {

    String multiplayer;

    @JsonProperty("user-support")
    String userSupport;
    String ads;

    public String getMultiplayer() {
        return multiplayer;
    }

    public void setMultiplayer(String multiplayer) {
        this.multiplayer = multiplayer;
    }

    public String getUserSupport() {
        return userSupport;
    }

    public void setUserSupport(String userSupport) {
        this.userSupport = userSupport;
    }

    public String getAds() {
        return ads;
    }

    public void setAds(String ads) {
        this.ads = ads;
    }

    /**
     * Overriding method equals to check if two StateDTO objects are the same.
     * We do not check equality for ads, since the response from external API changes.
     * */
    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof StateDTO)) {
            return false;
        }

        StateDTO c = (StateDTO) o;

        return c.multiplayer.equals(this.multiplayer) && c.userSupport.equals(this.userSupport);
    }

    @Override
    public String toString(){
        return String.format("Multiplayer: %s, User-Support: %s, Ads: %s", this.multiplayer, this.userSupport, this.ads);
    }

}
