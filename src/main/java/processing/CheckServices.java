package processing;

import beans.UserBean;
import com.kumuluz.ee.configuration.utils.ConfigurationUtil;
import entities.User;
import org.json.JSONObject;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.Authenticator;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class CheckServices {

    @Inject
    UserBean userBean;

    private Clock clock;

    @PostConstruct
    private void init(){
        initDefaultClock();
    }

    /***
     * Method that constructs response containing information about the enabled services.
     * @param timezone
     * @param userId
     * @param cc
     * @return
     */
    public StateDTO checkServices(String timezone, String userId, String cc){

        User user = userBean.getOrCreateUser(userId, cc, timezone);

        StateDTO state = new StateDTO();

        if(user.getNumberOfUses() > 5 && user.getCc().equals("US")){
            state.setMultiplayer("enabled");
        }else{
            state.setMultiplayer("disabled");
        }

        state.setUserSupport(getUserSupportStatus() ? "enabled" : "disabled");

        try {
            state.setAds(getAdsStatus(cc) ? "enabled" : "disabled");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    /***
     * Method that check if support personnel is available.
     * @return user-support status
     */
    private boolean getUserSupportStatus(){

        DayOfWeek currentDayInLjubljana = LocalDate.now(clock).getDayOfWeek();
        if(currentDayInLjubljana != DayOfWeek.SATURDAY && currentDayInLjubljana != DayOfWeek.SUNDAY){
            LocalDateTime currentTimeInLjubljana = LocalDateTime.now(clock);
            currentTimeInLjubljana = LocalDateTime.from(ZonedDateTime.of(currentTimeInLjubljana, ZoneId.of("Europe/Ljubljana")));
            if(currentTimeInLjubljana.toLocalTime().isAfter(LocalTime.of(8, 59)) && currentTimeInLjubljana.toLocalTime().isBefore(LocalTime.of(15,0))){
                return true;
            }
        }

        return false;
    }

    /***
     * Method that inquiries if external partner supports user device.
     * @param cc
     * @return ads status: boolean
     * @throws Exception
     */
    private boolean getAdsStatus(String cc) throws Exception {
        String url = ConfigurationUtil.getInstance().get("kumuluzee.ads.url").get();
        String username = ConfigurationUtil.getInstance().get("kumuluzee.ads.username").get();
        String password = ConfigurationUtil.getInstance().get("kumuluzee.ads.password").get();

        url = url + "/?countryCode="+cc;
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Authorization", "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes()))
                .build();


        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        if(response.statusCode() == 200){
            JSONObject responseBody = new JSONObject(response.body());

            if(responseBody.getString("ads").equals("sure, why not!")){
                return true;
            }
            return false;
        }else if(response.statusCode() == 400){
            throw new Exception("Missing mandatory parameters");
        }else if(response.statusCode() == 401){
            throw new Exception("Invalid credentials");
        }else{
            System.out.println(response);
            throw new Exception("Server is temporarily not available");
        }
    }

    /**
     * Initialization of clock.
     */
    public void initDefaultClock(){
        this.clock = Clock.system(Clock.systemDefaultZone().getZone());
    }

    public Clock getClock() {
        return clock;
    }

    public void setClock(Clock clock) {
        this.clock = clock;
    }
}
