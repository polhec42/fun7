package beans;

import entities.User;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class UserBean {

    @PersistenceContext(unitName = "fun7-jpa")
    private EntityManager em;

    public List<User> getUsers() {

        TypedQuery<User> query = em.createNamedQuery("User.getAll", User.class);
        return query.getResultList();
    }

    public User getUser(String userId){
        TypedQuery<User> query = em.createQuery("SELECT u from users u WHERE u.userId = :usedId", User.class);
        query.setParameter("usedId", userId);
        List<User> results = query.getResultList();
        if(results.size() == 0){
            return null;
        }
        return results.get(0);
    }

    @Transactional
    public User getOrCreateUser(String userId, String cc, String timezone){
        User user = getUser(userId);

        if(user != null){
            user.setNumberOfUses(user.getNumberOfUses()+1);
            user.setTimezone(timezone);
            user.setCc(cc);
            em.merge(user);
            return user;
        }

        user = new User();
        user.setUserId(userId);
        user.setCc(cc);
        user.setTimezone(timezone);
        user.setNumberOfUses(1);
        em.persist(user);
        return user;
    }

    @Transactional
    public boolean deleteUser(String userId){
        User user = getUser(userId);
        if(user != null) {
            em.remove(user);
            return true;
        }
        return false;
    }
}
