package api.v1;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@OpenAPIDefinition(
        info = @Info(
                title = "Fun7 REST API",
                version = "v1", contact = @Contact(email = "zan.magerl@hotmail.com"),
                description = "API for backend application for mobile game Fun7"),
        servers = @Server(url ="http://localhost:8080/v1"))
@ApplicationPath("v1")
public class ApplicationFun7 extends Application {

}
