package api.v1.sources;

import com.kumuluz.ee.rest.beans.QueryParameters;
import entities.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import processing.CheckServices;
import processing.StateDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("check")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@Tag(name = "Check services API")
public class CheckServicesSource {

    @Inject
    private CheckServices checkServices;

    @Operation(description = "Returns information about enabled services", responses = {
            @ApiResponse(responseCode = "200",
                    description = "Information about the enabled services. If user with such user string id does not exist, it is created. Timezone and country code of the user is updated if it's different from the current information.",
                    content = @Content(
                            schema = @Schema(implementation = StateDTO.class))
            ),
            @ApiResponse(responseCode = "400",
                    description = "Missing mandatory parameters"
            )})
    @GET
    public Response checkServices(@Parameter(description = "timezone of the user")  @QueryParam("timezone") String timezone, @Parameter(description = "country code of the user")@QueryParam("cc") String cc, @Parameter(description = "string id of the user") @QueryParam("userId") String userId){
        if(timezone == null || cc == null || userId == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.status(Response.Status.OK).entity(checkServices.checkServices(timezone, userId, cc)).build();
    }
}
