package api.v1.sources;


import beans.UserBean;
import entities.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("admin/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped

@Tag(name = "Admin API")
public class UserSource {

    @Inject
    UserBean userBean;

    @Operation(description = "Returns a list of users", responses = {
            @ApiResponse(responseCode = "200",
                    description = "List of users",
                    content = @Content(
                            array = @ArraySchema(schema = @Schema(implementation = User.class)))
            )})
    @GET
    public Response getUsers(){
        List<User> users = userBean.getUsers();
        return Response.status(Response.Status.OK).entity(users).build();
    }

    @Operation(description = "Returns details of the requested user", responses = {
            @ApiResponse(responseCode = "200",
                    description = "Returned details of the user",
                    content = @Content(
                            schema = @Schema(implementation = User.class))
            ),
            @ApiResponse(responseCode = "400",
                    description = "Id of the user was not provided."
            ),
            @ApiResponse(responseCode = "404",
                    description = "User with given id does not exist."
            )})
    @GET
    @Path("{userId}")
    public Response getUserDetails(@Parameter(description = "String id of the user") @PathParam("userId") String userId){
        if(userId == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        User user = userBean.getUser(userId);
        if(user == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.status(Response.Status.OK).entity(user).build();
    }



    @Operation(description = "Deletes user", responses = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully deleted user"
            ),
            @ApiResponse(responseCode = "400",
                    description = "Id of the user was not provided."
            ),
            @ApiResponse(responseCode = "404",
                    description = "User with given id was not deleted, since it did not exist"
            )
            })
    @DELETE
    @Path("{userId}")
    public Response deleteUser(@Parameter(description = "String id of the user") @PathParam("userId") String userId){
        if(userId == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        boolean deleted = userBean.deleteUser(userId);
        if(deleted){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
